#!/usr/bin/python2.7

''' Confuga HTTP - Main '''

from tornado.options     import define, options
from confuga.application import ConfugaApplication

# Main execution

if __name__ == '__main__':
    define('port', default=ConfugaApplication.HTTP_PORT, help='HTTP Port to listen on')

    options.parse_command_line()

    confuga_httpd = ConfugaApplication(port=options.port)
    confuga_httpd.run()

# vim: sts=4 sw=4 ts=8 expandtab ft=python
