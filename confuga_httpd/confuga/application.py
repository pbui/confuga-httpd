''' Confuga HTTP - Application '''

import os

import tornado.ioloop
import tornado.web

from confuga.database import ConfugaDatabase
from confuga.handler  import StorageNodesHandler

# Confuga Application

class ConfugaApplication(tornado.web.Application):
    HTTP_PORT = 9876

    def __init__(self, port=None, **settings):
        settings.update({
            'debug'         : True,
            'template_path' : os.path.join(os.path.dirname(__file__), 'templates'),
        })
        tornado.web.Application.__init__(self, **settings)

        self.port     = port or self.HTTP_PORT
        self.ioloop   = tornado.ioloop.IOLoop.instance() 
        self.database = ConfugaDatabase()

        self.add_handlers('', [
            (r'/StorageNodes', StorageNodesHandler)
        ])

        self.listen(self.port, xheaders=True)

    def run(self):
        self.ioloop.start()

# vim: sts=4 sw=4 ts=8 expandtab ft=python
