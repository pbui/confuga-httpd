''' Confuga HTTP - Utilities'''

# Confuga Exception

class ConfugaException(Exception):
    pass

# vim: sts=4 sw=4 ts=8 expandtab ft=python
