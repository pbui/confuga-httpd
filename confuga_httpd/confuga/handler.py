''' Confuga HTTP - Handlers '''

import tornado.web

# Confuga HTTP Request Handers

class StorageNodesHandler(tornado.web.RequestHandler):
    FIELDS = ('id', 'hostport', 'address', 'root', 'backend')

    def get(self):
        storage_nodes = self.application.database.storage_nodes()
        self.render('table.tmpl', data=storage_nodes, fields=self.FIELDS)

# vim: sts=4 sw=4 ts=8 expandtab ft=python
