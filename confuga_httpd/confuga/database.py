''' Confuga HTTP - Database '''

import os
import sqlite3

from confuga.util import ConfugaException

# Confuga Databases

class ConfugaDatabase(object):
    CONFUGA_PATH = 'confuga.db'
    JOB_PATH     = 'job.db'

    def __init__(self, db_paths=None):
        self.db_paths = db_paths or (self.CONFUGA_PATH, self.JOB_PATH)
        self.db_conn  = sqlite3.connect(':memory:')
        self.db_conn.row_factory = sqlite3.Row

        self._attach_databases()

    def _attach_databases(self):
        for db_path in self.db_paths:
            if not os.path.exists(db_path):
                raise ConfugaException('Database {0} does not exist'.format(db_path))

            db_title = os.path.splitext(db_path)[0].title()
            self.db_conn.execute('ATTACH ? AS ?', (db_path, db_title))

    def storage_nodes(self, active=False, limit=0):
        # TODO: handle limit
        cursor  = self.db_conn.cursor()
        table   = 'Confuga.StorageNodeActive' if active else 'Confuga.StorageNode'
        command = 'SELECT * FROM {0}'.format(table)

        for node in cursor.execute(command):
            yield node

# vim: sts=4 sw=4 ts=8 expandtab ft=python
